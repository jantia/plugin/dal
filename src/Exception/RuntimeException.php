<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Dal
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
namespace Jantia\Plugin\Dal\Exception;

/**
 *
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface {

}
